FROM ubuntu:16.04

ENV DCMCL_ENV=prod

# Set working directory
WORKDIR /opt/dcmc_lite_client

# Add requirements file
ADD requirements.txt ./requirements.txt

# Update and install packages and requirements
RUN apt-get update && apt-get install -y --no-install-recommends \
    netbase \
    openssh-client \
    openvpn \
    python3-dev \
    python3-setuptools \
    python3-paramiko \
    python3-pip \
    python3-wheel \
    redis-server \
    supervisor \
 && pip3 install -r requirements.txt \
 && apt-get remove -y python3-pip \
 && apt-get autoremove -y \
 && rm -rf /var/lib/apt/lists/* /root/.cache

# Copy source code
COPY . /opt/dcmc_lite_client

# Setup
RUN rm -rf /etc/supervisor/supervisord.conf \
 && cp config/supervisor/supervisord.conf /etc/supervisor/supervisord.conf \
 && cp config/supervisor/dcmc_lite_client.conf /etc/supervisor/conf.d/dcmc_lite_client.conf \
 && chmod +x config/docker/run.sh

# Gunicorn configuration
# Full settings can be found here: http://docs.gunicorn.org/en/stable/settings.html
ENV GUNICORN_CMD_ARGS="--bind 0.0.0.0:60004 --workers 4 --error-logfile - --access-logfile - --log-level=debug"

# Expose ports
EXPOSE 60004 60005

# Docker container entrypoint
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["/opt/dcmc_lite_client/config/docker/run.sh $DCMCL_ENV"]
