#!/usr/bin/env bash

# Start Redis-Server
service redis-server start && service redis-server status

# Supervisor
supervisord -c /etc/supervisor/supervisord.conf
update-rc.d supervisor defaults

# Start Gunicorn Server
gunicorn dcmc_lite_client.dcmcl:app

echo "Initialization completed."
tail -f /dev/null  # Necessary in order for the container to not stop
