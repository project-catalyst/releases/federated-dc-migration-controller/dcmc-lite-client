from http import HTTPStatus
from logging import getLogger
from urllib.parse import urljoin

from dcmc_lite_api_wrapper.http_client.client import Client


class DCMCLiteClient(object):
    """DCMC Lite Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Lite (DCMCL) Client components of the
    H2020 CATALYST Migration Controller. It covers calls to the entire available API of the DCMC Lite Clients.

    Methods
    -------
    get_public_ssh_key(tag)
        Retrieve the public SSH key of a DCMC Lite Client
    add_authorized_key(ssh_key, tag)
        Add a public SSH key to the authorized keys of DCMC Lite Client
    vpn_connect(tag)
        Command the DCMC Lite to connect to VPN
    vpn_disconnect(tag)
        Command the DCMC Lite to disconnect from VPN
    vxlan_configure(transaction_id, local_ip, remote_ip, tag)
        Configure VXLAN interface for live migration

    Examples
    --------
    `Initializing a DCMC Lite Client`

    >>> from dcmc_lite_api_wrapper.dcmcl_api import DCMCLiteClient
    >>> dcmcl = DCMCLiteClient(dcmcl_host_url=DCMCL_HOST_URL)

    `Get public SSH key`

    >>> dcmcl.get_public_ssh_key()

    `Authorized SSH key`

    >>> dcmcl.add_authorized_key(payload=SSH_KEY_PAYLOAD)

    `Connect to VPN for transaction`

    >>> dcmcl.vpn_connect(transaction_id=TRANSACTION_ID)

    `Disconnect from VPN for transaction`

    >>> dcmcl.vpn_disconnect(transaction_id=TRANSACTION_ID)

    `Configure VXLAN Interface`

    >>> dcmcl.vxlan_configure(transaction_id=TRANSACTION_ID, local_ip=VXLAN_LOCAL_IP, remote_ip=VXLAN_REMOTE_IP)

    Examples are provided in each of the available methods as well.

    """
    __API_PREFIX = 'catalyst/dcmcl/api/'
    __ADD_AUTHORIZED_KEY = 'ssh-keys/authorized-keys/add/'
    __RETRIEVE_PUBLIC_KEY = 'ssh-keys/public-key/retrieve/'
    __VPN_CONNECT = 'transaction/{transaction_id}/vpn/connect/'
    __VPN_DISCONNECT = 'transaction/{transaction_id}/vpn/disconnect/'
    __VXLAN_CONFIGURE = 'transaction/{transaction_id}/vxlan/configure/'

    def __init__(self, dcmcl_host_url):
        """DCMC Lite Client Class Constructor.

        Parameters
        ----------
        dcmcl_host_url : URL
            The DCMC Lite's Host URL

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin('http://{}'.format(dcmcl_host_url), DCMCLiteClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        self.log = getLogger()

    def get_public_ssh_key(self, tag='', role=''):
        """Retrieves the public SSH Key from the DCMC Lite Client.

        Parameters
        ----------
        tag : str
            A logging tag
        role : {'source', 'destination'}
            The role of the DCMC Lite's CMP

        Returns
        -------
        dict, None
            The response payload if request succeeds, `None` otherwise

        Examples
        --------
        >>> from dcmc_lite_api_wrapper.dcmcl_api import DCMCLiteClient
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url=DCMCL_HOST_URL)
        >>> dcmcl.get_public_ssh_key()
        {
            'public-key': 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAUb4kLdNgUtOphegkUHAJRQ== user@ubuntu',
            'host_ip': '127.0.0.1',
            'hostname': 'ubuntu'
        }

        """
        self.log.info('{} Requesting SSH key from {} CMP'.format(tag, role))
        url = urljoin(self.__url, DCMCLiteClient.__RETRIEVE_PUBLIC_KEY)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} Could not retrieve SSH key from {} CMP'.format(tag, role))
            self.log.debug('{} Error: `{}`'.format(tag, response.json()['error']))
            return None
        self.log.info('{} SSH Key was retrieved from {} CMP'.format(tag, role))
        return response.json()

    def add_authorized_key(self, payload, tag='', role=''):
        """Add a SSH Key to the authorized keys.

        Parameters
        ----------
        payload : dict
            A payload including the following fields:
                public-key : str
                    A public SSH key to authorize
                host_ip : str
                    The host ip to add to known_hosts
                hostname : str
                    The hostname of the SSH-key
        tag : str
            A logging tag
        role : {'source', 'destination'}
            The role of the DCMC Lite's CMP

        Returns
        -------
        bool
            `True` if authorization succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_lite_api_wrapper.dcmcl_api import DCMCLiteClient
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url=DCMCL_HOST_URL)
        >>> ssh_key_payload = {
        ...     'public-key': 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAUb4kLdNgUtOphegkUHAJRQ== user@ubuntu',
        ...     'host_ip': '127.0.0.1',
        ...     'hostname': 'ubuntu'
        ... }
        >>> dcmcl.add_authorized_key(payload=ssh_key_payload)
        True

        """
        self.log.info('{} Authorizing SSH-key in {}'.format(tag, role))
        url = urljoin(self.__url, DCMCLiteClient.__ADD_AUTHORIZED_KEY)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} SSH Key could not be authorized in {} CMP'.format(tag, role))
            self.log.debug('{} Error: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} SSH-Key was authorized in {} CMP'.format(tag, role))
        return True

    def vpn_connect(self, transaction_id, tag='', role=''):
        """Command DCMCL to connect to VPN.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to connect to VPN for
        tag : str
            A logging tag
        role : {'source', 'destination'}
            The role of the DCMC Lite's CMP

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_lite_api_wrapper.dcmcl_api import DCMCLiteClient
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url=DCMCL_HOST_URL)
        >>> dcmcl.vpn_connect(transaction_id=TRANSACTION_ID)
        True

        """
        self.log.info('{} Sending VPN connection request to {} CMP'.format(tag, role))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCLiteClient.__VPN_CONNECT).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code not in [HTTPStatus.OK.value, HTTPStatus.ACCEPTED.value]:
            self.log.error('{} VPN connection request failed. Status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Request for VPN connection has been accepted by the {} CMP'.format(tag, role))
        return True

    def vpn_disconnect(self, transaction_id, tag='', role=''):
        """Command DCMCL to disconnect from the VPN.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to connect to VPN for
        tag : str
            A logging tag
        role : {'source', 'destination'}
            The role of the DCMC Lite's CMP

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_lite_api_wrapper.dcmcl_api import DCMCLiteClient
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url=DCMCL_HOST_URL)
        >>> dcmcl.vpn_disconnect(transaction_id=TRANSACTION_ID)
        True

        """
        self.log.info('{} Sending VPN disconnection request to {} CMP'.format(tag, role))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCLiteClient.__VPN_DISCONNECT).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code not in [HTTPStatus.OK.value, HTTPStatus.ACCEPTED.value]:
            self.log.error('{} VPN disconnection request failed. Status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Request for VPN disconnection has been accepted by the {} CMP'.format(tag, role))
        return True

    def vxlan_configure(self, transaction_id, local_ip=None, remote_ip=None, tag=''):
        """Configure VXLAN interface of migration.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to connect to VPN for
        local_ip : str
            The local IP of the VXLAN interface
        remote_ip : str
            The remote IP of the VXLAN interface
        tag : str
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_lite_api_wrapper.dcmcl_api import DCMCLiteClient
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url=DCMCL_HOST_URL)
        >>> dcmcl.vxlan_configure(transaction_id=TRANSACTION_ID, local_ip=VXLAN_LOCAL_IP, remote_ip=VXLAN_REMOTE_IP)
        True

        """
        self.log.info('{} Requesting VXLAN configuration from [source] DCMC Lite ...'.format(tag))
        params = {'transaction_id': transaction_id}
        payload = {'local_ip': local_ip, 'remote_ip': remote_ip}
        url = urljoin(self.__url, DCMCLiteClient.__VXLAN_CONFIGURE).format(**params)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.ACCEPTED.value:
            self.log.error('{} VXLAN configuration request was not accepted by [source] CMP. Aborting'.format(tag))
            self.log.debug('{} Error: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} VXLAN configuration has been accepted by [source] DCMC Lite.'.format(tag))
        return True
