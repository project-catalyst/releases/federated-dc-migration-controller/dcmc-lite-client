from apispec import APISpec

from flask_apispec import doc, marshal_with, use_kwargs
from marshmallow import fields

from dcmc_lite_client.utils.miscellaneous import generate_composed_decorator
from dcmc_lite_client.schemas.dcmcl import ssh_key_schema

# ==================================
#      API DESCRIPTION & SPEC
# ==================================
API_DESCRIPTION = """The API exposed by the DCMC Lite Client component (H2020 Catalyst Project).

The `swagger-ui` view can be found [here](/catalyst/dcmcl/api/docs).  
The `swagger-json` view can be found [here](/catalyst/dcmcl/api/swagger.json).  

LGPLv3
"""

spec = APISpec(
    title='[H2020 CATALYST] DCMC Lite Client API',
    version='v1',
    openapi_version='3.0.2',
    info=dict(description=API_DESCRIPTION)
)

# ==================================
#        SSH-KEY ENDPOINTS
# ==================================
add_key_doc = generate_composed_decorator(
    doc(description='Add SSH Key to authorized keys', tags=['ssh-keys']),
    use_kwargs({'public-key': fields.Str(), 'host_ip': fields.Str(), 'hostname': fields.Str()}),
    marshal_with(None, 200, 'Key was added to authorized keys'),
    marshal_with(None, 400, 'Invalid of malformed parameters'),
    marshal_with(None, 500, 'Internal Server Error')
)
retrieve_key_doc = generate_composed_decorator(
    doc(description='Retrieve the host\'s public SSH Key', tags=['ssh-keys']),
    marshal_with(ssh_key_schema, 200, 'Requested key was retrieved successfully.'),
    marshal_with(None, 500, 'Internal Server Error')
)

# ==================================
#         VPN ENDPOINTS
# ==================================
vpn_connect_doc = generate_composed_decorator(
    doc(description='Connect to VPN', tags=['transaction']),
    marshal_with(None, 202, 'VPN Connection request is being processed.'),
    marshal_with(None, 200, 'This DCMC Lite is already connected to VPN.')
)
vpn_disconnect_doc = generate_composed_decorator(
    doc(description='Disconnect from VPN', tags=['transaction']),
    marshal_with(None, 202, 'VPN disconnected successfully'),
    marshal_with(None, 500, 'Internal Server Error')
)

# ==================================
#          VXLAN ENDPOINT
# ==================================
vxlan_config_doc = generate_composed_decorator(
    doc(description='Configure VXLAN iface', tags=['transaction']),
    use_kwargs({'local_ip': fields.Str(), 'remote_ip': fields.Str()}),
    marshal_with(None, 202, 'VXLAN interface is being configured'),
    marshal_with(None, 400, 'Invalid of malformed parameters'),
    marshal_with(None, 500, 'Internal Server Error')
)
