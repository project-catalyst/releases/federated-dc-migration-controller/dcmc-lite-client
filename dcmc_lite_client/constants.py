# ==================================
#        REQUEST RETRIALS
# ==================================
MAX_RETRIES = 5

# ==================================
#             LOGGING
# ==================================
CELERY_TASK_LOGGER = 'task_logger'
API_LOGGER = 'api_logger'

# ==================================
#       DC INSTALLATION TYPE
# ==================================
NORMAL = 'distributed'
ALL_IN_ONE = 'all_in_one'

# ==================================
#       TYPES OF CMP HOSTS
# ==================================
PHYSICAL = 'physical'
VIRTUAL = 'virtual'

# ==================================
#            API PREFIX
# ==================================
API_PREFIX = '/catalyst/dcmcl/api'

# ==================================
#    CONFIGURATION COMMAND
# ==================================
CONFIG_CMD = "sed '" \
             "s,OS_AUTHENTICATE_URI,{authenticate_uri},g; " \
             "s,OS_AUTHORIZATION_URL,{authorization_url},g; " \
             "s,OS_GLANCE_SERVICE,{glance_service},g; " \
             "s,OS_MEMCACHED_SERVICE,{memcached_service},g; " \
             "s,OS_MEMCACHED_SECRET_KEY,{memcached_secret_key},g; " \
             "s,OS_NEUTRON_SERVICE,{neutron_service},g; " \
             "s,OS_NEUTRON_PASS,{neutron_password},g; " \
             "s,OS_NOVA_PASS,{nova_password},g; " \
             "s,OS_PLACEMENT_SERVICE,{placement_service},g; " \
             "s,OS_PLACEMENT_PASS,{placement_password},g; " \
             "s,OS_TRANSPORT_URL,{transport_url},g; " \
             "s,OS_LIBVIRT_USER,{libvirt_user},g; " \
             "s,DCMCL_VPN_IP,{vpn_ip},g' " \
             "/opt/dcmc_lite_client/os_config/nova/nova.conf > " \
             "/opt/dcmc_lite_client/os_config/nova/temp_nova.conf && " \
             "sed '" \
             "s,OS_AUTHORIZATION_URL,{authorization_url},g; " \
             "s,OS_MEMCACHED_SERVICE,{memcached_service},g; " \
             "s,OS_MEMCACHED_SECRET_KEY,{memcached_secret_key},g; " \
             "s,OS_NEUTRON_PASS,{neutron_password},g; " \
             "s,OS_NOVA_PASS,{nova_password},g; " \
             "s,OS_TRANSPORT_URL,{neutron_transport},g; " \
             "s,DCMCL_VPN_IP,{vpn_ip},g' " \
             "/opt/dcmc_lite_client/os_config/neutron/neutron.conf > " \
             "/opt/dcmc_lite_client/os_config/neutron/temp_neutron.conf && " \
             "sed '" \
             "s,DCMCL_VPN_IP,{vpn_ip},g' " \
             "/opt/dcmc_lite_client/os_config/neutron/plugins/ml2/openvswitch_agent.ini > " \
             "/opt/dcmc_lite_client/os_config/neutron/plugins/ml2/temp_openvswitch_agent.ini && " \
             "sleep 5 && " \
             "echo '{nova_uid}' >> /opt/dcmc_lite_client/os_config/uid/nova.txt && " \
             "echo '{nova_gid}' >> /opt/dcmc_lite_client/os_config/gid/nova.txt && " \
             "echo '{libvirt_uid}' >> /opt/dcmc_lite_client/os_config/uid/libvirt-qemu.txt && " \
             "echo '{libvirt_gid}' >> /opt/dcmc_lite_client/os_config/gid/kvm.txt && " \
             "echo '{libvirt_group}' >> /opt/dcmc_lite_client/os_config/gid/libvirt.txt && " \
             "echo '127.0.0.1 {compute_hostname}' >> /opt/dcmc_lite_client/hosts && " \
             "echo '127.0.1.1 {compute_hostname}' >> /opt/dcmc_lite_client/hosts && " \
             "echo '{vpn_ip} {compute_hostname}' >> /opt/dcmc_lite_client/hosts && " \
             "echo '{nfs_server_host}:{nfs_server_root} /var/lib/nova/instances nfs4 defaults 0 0' | tee -a fstab && " \
             "echo '{dc_ip} controller' >> /opt/dcmc_lite_client/hosts"

# ==================================
#          OVSDB ADDRESS
# ==================================
OVSDB_ADDR = 'tcp:127.0.0.1:6640'
