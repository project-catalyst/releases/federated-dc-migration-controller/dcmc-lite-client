import logging
import logging.config
import subprocess

import os
from flask import Flask, request, url_for
from flask_api import status
from flask_apispec import FlaskApiSpec
from flask_cors import CORS
from jsonschema import validate, FormatChecker, ValidationError

from dcmc_lite_client import constants, settings
from dcmc_lite_client.schemas.dcmcl import vxlan_config_schema
from dcmc_lite_client.swagger.essentials import *
from dcmc_lite_client.utils.miscellaneous import get_or_create_public_ssh_key, get_ip_of_interface, get_hostname
from tasks import do_connect_to_vpn_task, remove_vpn, register_vcmp, get_and_set_vxlan_iface

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(constants.API_LOGGER)

# Init Flask app and config
app = Flask(__name__)
app.config['APISPEC_SWAGGER_URL'] = constants.API_PREFIX + '/swagger.json'
app.config['APISPEC_SWAGGER_UI_URL'] = constants.API_PREFIX + '/docs'
CORS(app)


@add_key_doc
@app.route(constants.API_PREFIX + '/ssh-keys/authorized-keys/add/', methods=['POST'])
def authorize_ssh_key():
    logger.info('[{}] Received request for SSH-key authorization'.format(url_for('authorize_ssh_key')))
    public_key = request.json
    try:
        validate(instance=public_key, schema=ssh_key_schema, format_checker=FormatChecker())
    except ValidationError as e:
        logger.error('[{}] Invalid or malformed data: `{}`'.format(url_for('authorize_ssh_key'), e))
        payload = {'error': 'Invalid of malformed data: {}'.format(e)}
        return payload, status.HTTP_400_BAD_REQUEST
    except Exception as e:
        logger.error('[{}] A server error has occurred: `{}`'.format(url_for('authorize_ssh_key'), e))
        payload = {'error': 'A server error occurred: {}'.format(e)}
        return payload, status.HTTP_500_INTERNAL_SERVER_ERROR
    subprocess.Popen('echo {public-key} >> user_ssh_dir/authorized_keys && '
                     'echo {host_ip} {hostname} >> hosts && cp hosts /etc/hosts && '
                     'ssh-keyscan -H {host_ip} | tee -a root_ssh_dir/known_hosts && '
                     'ssh-keyscan -H {hostname} | tee -a root_ssh_dir/known_hosts'
                     .format(**public_key), shell=True)
    logger.info('[{}] SSH-key has been authorized'.format(url_for('authorize_ssh_key')))
    payload = {'success': 'Key was added to the authorized keys.'}
    return payload, status.HTTP_200_OK


@retrieve_key_doc
@app.route(constants.API_PREFIX + '/ssh-keys/public-key/retrieve/', methods=['GET'])
def retrieve_ssh_key():
    logger.info('[{}] Received request for SSH-key retrieval'.format(url_for('retrieve_ssh_key')))
    try:
        public_key = get_or_create_public_ssh_key().strip()
    except Exception as e:
        logger.error('[{}] A server error has occurred: `{}`'.format(url_for('retrieve_ssh_key'), e))
        payload = {'error': 'A server error occurred: {}'.format(e)}
        return payload, status.HTTP_500_INTERNAL_SERVER_ERROR
    logger.info('[{}] SSH-key has been retrieved'.format(url_for('retrieve_ssh_key')))
    payload = {'public-key': public_key, 'host_ip': get_ip_of_interface(os.getenv('DCMCL_TUN_DEVICE')),
               'hostname': get_hostname()}
    return payload, status.HTTP_200_OK


@vpn_connect_doc
@app.route(constants.API_PREFIX + '/transaction/<int:transaction_id>/vpn/connect/', methods=['GET'])
def vpn_connection(transaction_id):
    logger.info('[{}] Received VPN connection request'
                .format(url_for('vpn_connection', transaction_id=transaction_id)))
    if settings.DCMCL_DC_INSTALLATION_TYPE == constants.NORMAL:
        do_connect_to_vpn_task.delay(transaction_id)
        logger.info('[{}] Request for VPN connection is being processed'
                    .format(url_for('vpn_connection', transaction_id=transaction_id)))
        payload = {'success': 'Request for VPN connection is currently processed.'}
        return payload, status.HTTP_202_ACCEPTED
    logger.info('[{}] This CMP is already connected to VPN'
                .format(url_for('vpn_connection', transaction_id=transaction_id)))
    register_vcmp.delay(transaction_id)
    payload = {'success': 'This DCMC Lite Client is already connected to VPN.'}
    return payload, status.HTTP_200_OK


@vpn_disconnect_doc
@app.route(constants.API_PREFIX + '/transaction/<int:transaction_id>/vpn/disconnect/', methods=['GET'])
def vpn_disconnection(transaction_id):
    logger.info('[{}] This CMP is already connected to VPN'
                .format(url_for('vpn_disconnection', transaction_id=transaction_id)))
    if settings.DCMCL_DC_INSTALLATION_TYPE == constants.ALL_IN_ONE:
        logger.info('[{}] This CMP is not independently connected to VPN'
                    .format(url_for('vpn_disconnection', transaction_id=transaction_id)))
        payload = {'success': 'This DCMC Lite is not independently connected to a VPN network.'}
        return payload, status.HTTP_200_OK
    try:
        remove_vpn.delay(transaction_id)
    except Exception as e:
        logger.error('[{}] An server error has occurred: `{}`'
                     .format(url_for('vpn_disconnection', transaction_id=transaction_id), e))
        payload = {'error': 'A server error occurred: {}'.format(e)}
        return payload, status.HTTP_500_INTERNAL_SERVER_ERROR
    logger.info('[{}] VPN connection for CMP is being removed'
                .format(url_for('vpn_disconnection', transaction_id=transaction_id)))
    payload = {'success': 'VPN connection is being removed'}
    return payload, status.HTTP_202_ACCEPTED


@vxlan_config_doc
@app.route(constants.API_PREFIX + '/transaction/<int:transaction_id>/vxlan/configure/', methods=['POST'])
def vxlan_configuration(transaction_id):
    logger.info('[{}] Received VXLAN Configuration Request'
                .format(url_for('vxlan_configuration', transaction_id=transaction_id)))
    vxlan_config_payload = request.json
    try:
        validate(instance=vxlan_config_payload, schema=vxlan_config_schema, format_checker=FormatChecker())
        local_ip = vxlan_config_payload['local_ip']
        remote_ip = vxlan_config_payload['remote_ip']
        get_and_set_vxlan_iface.delay(transaction_id, local_ip, remote_ip)
    except ValidationError as e:
        logger.error('[{}] Invalid or malformed data: `{}`'
                     .format(url_for('vxlan_configuration', transaction_id=transaction_id), e))
        payload = {'error': 'Invalid of malformed data: {}'.format(e)}
        return payload, status.HTTP_400_BAD_REQUEST
    except Exception as e:
        logger.error('[{}] An server error has occurred: `{}`'
                     .format(url_for('vxlan_configuration', transaction_id=transaction_id), e))
        payload = {'error': 'A server error occurred: {}'.format(e)}
        return payload, status.HTTP_500_INTERNAL_SERVER_ERROR
    logger.info('[{}] VXLAN interface is being configured.'
                .format(url_for('vxlan_configuration', transaction_id=transaction_id)))
    payload = {'success': 'VXLAN interface is being configured'}
    return payload, status.HTTP_202_ACCEPTED


# Setup Docs
docs = FlaskApiSpec(app)
docs.spec = spec
docs.register(authorize_ssh_key)
docs.register(retrieve_ssh_key)
docs.register(vpn_connection)
docs.register(vpn_disconnection)
docs.register(vxlan_configuration)
