"""Settings for dcmc_lite_client project. """
import os
import socket

# ==================================
#           PROJECT ROOT
# ==================================
PROJECT_ROOT = '/opt/dcmc_lite_client'

# ==================================
#         TIMEZONE SETTINGS
# ==================================
TIME_ZONE = 'UTC'
USE_TZ = True

# ==================================
#       MULTILINGUAL SETTINGS
# ==================================
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True

# ==================================
#         LOGGING SETTINGS
# ==================================
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] - [%(name)s:%(lineno)s] - [%(levelname)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'task_logger': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': str(PROJECT_ROOT) + "/logs/celery-tasks.log",
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'api_logger': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': str(PROJECT_ROOT) + "/logs/api.log",
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'task_logger': {
            'handlers': ['task_logger'],
            'level': 'DEBUG',
        },
        'api_logger': {
            'handlers': ['api_logger'],
            'level': 'DEBUG',
        }
    }
}

# =================================
#       OPENVPN CONNECTION
# =================================
OVPN_FILE_NAME = 'openvpn-{}.ovpn'.format(socket.gethostname())
WORKING_DIR = os.getcwd()
OVPN_FILE_LOCATION = os.path.join(WORKING_DIR, OVPN_FILE_NAME)

# =================================
#   DATA RECEIVED FROM DCMCS
# =================================
ITLM_TRANSACTION_ID = os.getenv('DCMCL_TRANSACTION_ID')

# =================================
#     DCMC CMP DEPLOYMENT TYPE
# =================================
DCMCL_DC_INSTALLATION_TYPE, DCMCL_CMP_TYPE = os.getenv('DCMCL_DC_HOST_TYPE').split(':')
