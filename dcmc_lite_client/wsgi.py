from dcmc_lite_client.dcmcl import app

if __name__ == "__main__":
    app.run(host='0.0.0.0')
