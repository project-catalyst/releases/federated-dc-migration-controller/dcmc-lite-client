vpn_details_schema = {
    'type': 'object',
    'properties': {
        'vpn_ip': {
            'type': 'string'
        },
        'dcmc1m_ip': {
            'type': 'string'
        }
    },
    'required': ['vpn_ip', 'dcmc1m_ip']
}

ssh_key_schema = {
    'type': 'object',
    'properties': {
        'public-key': {
            'type': 'string'
        },
        'host_ip': {
            'type': 'string'
        },
        'hostname': {
            'type': 'string'
        }
    },
    'required': ['public-key', 'host_ip', 'hostname']
}

vxlan_config_schema = {
    'type': 'object',
    'properties': {
        'local_ip': {
            'type': 'string'
        },
        'remote_ip': {
            'type': 'string'
        }
    },
    'required': ['local_ip', 'remote_ip']
}
