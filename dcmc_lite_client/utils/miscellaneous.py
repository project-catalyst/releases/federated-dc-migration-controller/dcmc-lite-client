import netifaces as ni
import os
import socket
import subprocess
from urllib.parse import urlparse

from dcmc_lite_client.constants import CONFIG_CMD


def get_hostname():
    """Return the hostname.

    Returns
    -------
    hostname : str
        The hostname of the machine

    """
    return socket.gethostname()


def get_or_create_public_ssh_key():
    """Retrieves (or creates and retrieves) a public SSH Key.

    Returns
    -------
    public_ssh_key : str
        The public SSH key of the machine

    """
    with open('root_ssh_dir/id_rsa.pub') as file:
        public_ssh_key = file.read()
        return public_ssh_key


def get_ip_of_interface(interface):
    """Returns the IP of a network interface.

    Parameters
    ----------
    interface : str
        A network interface

    Returns
    -------
    ip_address : str
        The IP address of a given interface

    """
    try:
        ip_address = ni.ifaddresses(interface)[ni.AF_INET][0]['addr']
        return ip_address
    except (ValueError, KeyError):
        return None


def generate_composed_decorator(*decorators):
    """Combine decorators into one.

    Parameters
    ----------
    decorators
        The decorators to combine

    Returns
    -------
    decorator
        A composed decorator

    """

    def composed_decorator(f):
        for decorator in reversed(decorators):
            f = decorator(f)
        return f

    return composed_decorator


def update_nova_configuration(payload, dc_ip, dcmcl_vpn_ip):
    """Update the Nova configuration.

    Parameters
    ----------
    payload : dict
        The service endpoints and passwords
    dc_ip : str
        The IP of the controller node
    dcmcl_vpn_ip : str
        The VPN of the DCMC Lite Client

    """
    payload['dc_ip'] = dc_ip
    payload['vpn_ip'] = dcmcl_vpn_ip
    payload['local_ip'] = get_ip_of_interface(os.getenv('DCMCL_PHYSICAL_NI'))
    payload['compute_hostname'] = os.getenv('DCMCL_KEYCLOAK_USERNAME')
    payload['neutron_transport'] = '://'.join(urlparse(payload['transport_url'])[0:2])
    subprocess.Popen(CONFIG_CMD.format(**payload), shell=True)
