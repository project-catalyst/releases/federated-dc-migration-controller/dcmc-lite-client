import subprocess
import time
from os import path

from dcmc_lite_client import settings


def remove_last_line_from_string(s):
    temps = s[:s.rfind('\n')]
    certificate = temps[:temps.rfind('\n')] + "\ncomp-lzo"
    return certificate


def append_certificate_to_file(response):
    """Store certificate to a .ovpn file."""
    response_body = response.json()
    if 'certificate' in response_body.keys():
        changed_certificate = remove_last_line_from_string(response_body['certificate'])
        with open(settings.OVPN_FILE_LOCATION, 'w+') as f:
            f.write(changed_certificate)


def do_connect_to_vpn():
    """Uses .ovpn file and connects your local machine to VPN network."""
    while not path.isfile('./{}'.format(settings.OVPN_FILE_NAME)):
        time.sleep(5)
    command = 'openvpn --config {}'.format(settings.OVPN_FILE_NAME)
    subprocess.Popen(command, shell=True)
