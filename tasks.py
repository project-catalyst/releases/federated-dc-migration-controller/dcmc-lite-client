import ipaddress
import logging
import time
import subprocess

import os
from celery import Celery
from celery.signals import worker_ready
from flask_api import status
from ryu.lib.ovs import vsctl

from communication_handler.communication_handler import get_communication_handler
from dcmc_lite_client import constants
from dcmc_lite_client import settings
from dcmc_lite_client.constants import OVSDB_ADDR
from dcmc_lite_client.utils.miscellaneous import get_hostname, get_ip_of_interface, update_nova_configuration
from dcmc_lite_client.utils.vpn import append_certificate_to_file, do_connect_to_vpn

app = Celery('tasks', broker='redis://localhost:6379/0')
logger = logging.getLogger(constants.CELERY_TASK_LOGGER)


@worker_ready.connect()
def post_vcontainer_creation_to_dcmcs(**kwargs):
    """Informs DCMC Server of the VContainer creation on startup. """

    # This task in only used in virtual deployment (by the Destination DCMC Lite)
    if settings.DCMCL_CMP_TYPE == constants.PHYSICAL:
        return

    task_tag = 'task:post_vcontainer_creation_to_dcmcs'
    transaction_tag = 'Transaction:{}'.format(os.getenv('DCMCL_TRANSACTION_ID'))
    logger.info('[{}][{}] Informing DCMC Server of VContainer Creation'.format(task_tag, transaction_tag))

    # Inform DCMC Server of VContainer Creation
    response = get_communication_handler().dcmcs.vcontainer_created(os.getenv('DCMCL_TRANSACTION_ID'))
    if response.status_code in [status.HTTP_200_OK, status.HTTP_201_CREATED]:
        logger.info('[{}][{}] OK. Request to DCMC Server succeeded'.format(task_tag, transaction_tag))
    else:
        logger.warning('[{}][{}] Request failed with response code [{}]'
                       .format(task_tag, transaction_tag, response.status_code))
        logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.status_code))


@app.task
def do_connect_to_vpn_task(transaction_id):
    """Connects to an initialized VPN connection.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction as kept in the IT Load Marketplace

    """
    task_tag = 'task:connect_to_vpn'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Connecting to VPN ...'.format(task_tag, transaction_tag))

    # Initially the VPN details must be retrieved from the Cloud OpenVPN server. The details are
    # retrieved in the form of a certificate. The certificate is stored in the working directory
    # and is later used to connect to VPN through OpenVPN.

    while True:
        response = get_communication_handler().covpn.vpn_by_owner_and_tenant()
        if response.status_code == status.HTTP_200_OK:
            # The certificate has been received.
            append_certificate_to_file(response)
            break
        logger.warning('[{}][{}] The VPN connection creation is not completed yet. Waiting ...'
                       .format(task_tag, transaction_tag))
        logger.debug('[{}][{}] Response status code: [{}]'.format(task_tag, transaction_tag, response.status_code))
        time.sleep(10)
    logger.info('[{}][{}] VPN connection details were retrieved successfully'.format(task_tag, transaction_tag))
    do_connect_to_vpn()
    while True:
        time.sleep(2)
        try:
            ipaddress.IPv4Address(get_ip_of_interface(os.getenv('DCMCL_TUN_DEVICE')))
            break
        except ipaddress.AddressValueError:
            logger.warning('[{}][{}] The VPN connection is not completed yet. Waiting ...'
                           .format(task_tag, transaction_tag))
    logger.info('[{}][{}] VPN connection was established'.format(task_tag, transaction_tag))
    register_vcmp(transaction_id)


@worker_ready.connect()
def connect_to_vpn(**kwargs):
    """Connects to an initialized VPN connection. """
    if settings.DCMCL_CMP_TYPE == constants.VIRTUAL:
        do_connect_to_vpn_task(os.getenv('DCMCL_TRANSACTION_ID'))


@app.task
def register_vcmp(transaction_id):
    """Register the Virtual Compute Node with the source DC controller.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction as kept in the IT Load Marketplace

    """
    task_tag = 'task:register_vcmp'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Starting registration of vCMP node to source DC Controller'
                .format(task_tag, transaction_tag))

    # Get controller details from DCMC Server. DCMC Server will operate as a proxy in this
    # case because the host URL of the source DCMC is unknown to the DCMC Lite Client.
    while True:
        response = get_communication_handler().dcmcs.controller(transaction_id)
        if response.status_code != status.HTTP_200_OK:
            logger.warning('[{}][{}] The VPN IP of the controller could not be found'.format(task_tag, transaction_tag))
            time.sleep(15)
        elif response.status_code == status.HTTP_200_OK:
            logger.info('[{}][{}] OK. Successfully retrieved controller details'.format(task_tag, transaction_tag))
            controller_data = response.json()
            dc_ip = controller_data['ip']
            port = controller_data['port']
            os.environ['DCMCL_DCMCM_HOST_URL'] = 'http://{}:{}'.format(dc_ip, port)
            break
    # Upon echoing, the bash script will restart nova-compute
    if settings.DCMCL_CMP_TYPE == constants.VIRTUAL:
        # Get service endpoints from DCMC Master
        logger.info('[{}][{}] Retrieving service endpoints from DCMC Master'.format(task_tag, transaction_tag))
        response = get_communication_handler().dcmcm.service_endpoints(transaction_id)
        if response.status_code != status.HTTP_200_OK:
            logger.error('[{}][{}] Request failed with status code [{}] and response payload: `{}`'
                         .format(task_tag, transaction_tag, response.status_code, response.json()))
            return
        logger.info('[{}][{}] Service endpoints retrieved successfully'.format(task_tag, transaction_tag))
        update_nova_configuration(response.json(), dc_ip, get_ip_of_interface(os.getenv('DCMCL_TUN_DEVICE')))

    # Register
    logger.info('[{}][{}] Reporting readiness to DCMC Master'.format(task_tag, transaction_tag))
    payload = {'host_name': get_hostname(), 'vpn_ip': get_ip_of_interface(os.getenv('DCMCL_TUN_DEVICE'))}
    response = get_communication_handler().dcmcm.vcontainer_registered(transaction_id, payload)
    if response.status_code != status.HTTP_200_OK:
        logger.error('[{}][{}] Request failed with status code [{}] and response payload: `{}`'
                     .format(task_tag, transaction_tag, response.status_code, response.json()))
        return
    logger.info('[{}][{}] Compute node has reported its readiness to DCMC Master'.format(task_tag, transaction_tag))


@app.task
def remove_vpn(transaction_id):
    """Removes .ovpn file and stops VPN connection.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction as kept in the IT Load Marketplace

    """
    task_tag = 'task:remove_vpn'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Removing VPN connection ...'.format(task_tag, transaction_tag))

    # Remove .ovpn file and stop OpenVPN
    command_1 = 'rm -rf {}'.format(settings.OVPN_FILE_NAME)
    subprocess.Popen(command_1, shell=True)
    command_2 = 'pkill openvpn'
    subprocess.Popen(command_2, shell=True)


@app.task
def get_and_set_vxlan_iface(transaction_id, local_ip, remote_ip):
    """Retrieve VXLAN interface and set it's local IP.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction as kept in the IT Load Marketplace
    local_ip : str
        The local IP to set to VXLAN interface
    remote_ip : str
        The remote IP, already set to VXLAN interface

    """
    task_tag = 'task:get_and_set_vxlan_iface'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Retrieving and setting VXLAN interface ...'.format(task_tag, transaction_tag))

    # Connect to OVSDB
    ovs_vsctl = vsctl.VSCtl(OVSDB_ADDR)
    # Iterate over br-tun interfaces
    while True:
        # Search for VXLAN iface with a given remote IP
        find_iface_command = vsctl.VSCtlCommand('find', ('Interface', 'options:remote_ip={}'.format(remote_ip)))
        ovs_vsctl.run_command([find_iface_command])

        # If VXLAN iface is found, its local IP must be set in order for all VXLAN ifaces related to the
        # live migration to be in the same (VPN) network. Hence the local IP of the VXLAN is set to this
        # of the tun interface, through which DCMC Master (Lite) is connected to the VPN network.
        if find_iface_command.result:
            vxlan_iface = find_iface_command.result[0].name
            set_iface_command = \
                vsctl.VSCtlCommand('set', ('Interface', vxlan_iface, 'options:local_ip={}'.format(local_ip)))
            ovs_vsctl.run_command([set_iface_command])
            break
        logger.debug('[{}][{}] Requested VXLAN interface is not created yet ...'.format(task_tag, transaction_tag))
        time.sleep(1)
    logger.info('[{}][{}] Local IP of requested VXLAN interface is set'.format(task_tag, transaction_tag))
