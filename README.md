# DC Migration Controller Lite Client (DCMC Lite)
This is the repository for the DCMC Lite, implemented for the Migration Controller of the H2020 CATALYST Project. The 
DCMC Lite resides in each of the Compute (CMP) nodes of a Datacenter (DC) in the CATALYST Federation. In the physical
CMP nodes, DCMC Lite is normally deployed. Additionally, it is automatically deployed when the vCMP is brought up. The
DCMC Lite Clients are responsible for preparing the grounds for a live-migration, including the VPN connection, the 
SSH-key exchange and VXLAN configuration.

## Installation Guide

### Prerequisites

For the deployment of the DCMC Lite component the Docker engine as well as docker-compose should be installed. These 
actions can be performed following the instructions provided below. Firstly, an update should be performed and essential 
packages should be installed:

```bash
sudo apt-get update
sudo apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     software-properties-common
```

Secondly the key and Docker repository should be added:

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
```

Then another update is performed, Docker is installed and the user is added to docker group.

```bash
sudo apt-get update
sudo apt-get install -y docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
```

Finally, docker-compose should be installed:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Environmental Parameters
The DCMC Lite service, which is executed as a Docker container, receives its configuration from a .env file located in 
the root of the repository. In the following table, the environmental parameters that are necessary for the 
configuration and deployment of the DCMC Lite are recorded and described.

__Docker and Internal Settings__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `Docker` | The project name (default: catalyst) |
| DCMCL_DC_HOST_TYPE | `DCMC Lite` | The host type in the form <deployment_type>:<host_type>. Possible values: *deployment_type*: {all_in_one, distributed}, *host_type*: {physical, virtual} |
| DCMCL_PHYSICAL_NI | `DCMC Lite` | The machine's physical network interface |
| DCMCL_SSH_KEY_DIR | `DCMC Lite` | SSH directory to mount for SSH-key exchange (e.g. /opt/stack/.ssh)  |
| DCMCL_TUN_DEVICE | `DCMC Lite` | Temporary settings, until each transaction is mapped to a tun interface  |


__External Services__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| DCMCL_KEYCLOAK_USERNAME | `Keycloak` | Username |
| DCMCL_KEYCLOAK_PASSWORD | `Keycloak` | Password |
| DCMCL_COVPN_HOST_URL | `Cloud OpenVPN` | Host URL |
| DCMCL_COVPN_OWNER |`Cloud OpenVPN` | Connection Owner |
| DCMCL_COVPN_TENANT |`Cloud OpenVPN` | Tenant (default: Catalyst) |
| DCMCL_DCMCS_HOST_URL | `DCMC Server` | Host URL |


### Deployment

The DCMC Lite is deployed as a single Docker container, utilizing docker-compose. Having cloned the code of this 
repository, and having created the .env file the following commands should be executed:

```bash
cp .env /dcmc-lite-client
cd dcmc-lite-client
docker-compose up --build -d
```

It should be noted that the DCMC Lite Client is normally launched automatically during the preparation of the migration
when the VContainer which will host the OpenStack Virtual Compute Node is created. Hence, these installation 
instructions are given mostly for reference and/or testing.

## API

Here, the API that DCMC Lite Client offers is briefly documented. For more information you may check both the Swagger
documentation and the DCMC Lite Client Documentation, Release v1.0.0. The prefix of the API endpoints is **/catalyst/dcmcl/api**.

### Authorize SSH-key
| **URL**             |	/ssh-keys/authorized-keys/add/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json             |
| **Request body**	  | SSH-key                                    |
| **Response body**	  | N/A                                        |
| **Response codes**  | 200 – Key was added to authorized keys.<br>400 – The provided data is invalid or malformed.<br>500 – Internal server error |

### Retrieve SSH-key
| **URL**             |	/ssh-keys/authorized-keys/retrieve/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json             |
| **Request body**	  | N/A                                        |
| **Response body**	  | [SSH-key](#ssh-key)                        |
| **Response codes**  | 200 – Key was retrieved successfully.<br>500 – Internal server error |

### Connect to VPN
| **URL**             |	/transaction/{transaction_id}/vpn/connect/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json             |
| **Request body**	  | N/A                                        |
| **Response body**	  | N/A                                        |
| **Response codes**  | 200 – This DCMC Lite is already connected to VPN.<br>202 – VPN connection is being processed.<br>500 – Internal server error |

### Disconnect from VPN
| **URL**             |	/transaction/{transaction_id}/vpn/disconnect/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json             |
| **Request body**	  | N/A                                        |
| **Response body**	  | N/A                                        |
| **Response codes**  | 202 – VPN disconnected successfully.<br>500 – Internal server error |

### VXLAN Configuration
| **URL**             |	/transaction/{transaction_id}/vxlan/configure/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json             |
| **Request body**	  | [VXLAN-config](#vxlan-config)              |
| **Response body**	  | N/A                                        |
| **Response codes**  | 202 – VXLAN interface is being configured.<br>400 – The provided data is invalid or malformed<br>500 – Internal server error |

### Data Model

#### SSH-key
```json
{
   "required": ["public-key", "host_ip", "hostname"],
   "type": "object",
   "properties": {
      "public-key": {
         "title": "Public key",
         "description": "The public SSH-key to authorize",
         "type": "string"
      },
      "host_ip": {
         "title": "Host IP",
         "description": "The IP of the host under authorization",
         "type": "string",
         "maxLength": 15, 
         "minLength": 7
      },
      "hostname": {
         "title": "Hostname",
         "description": "The hostname of the host under authorization",
         "type": "string",
         "maxLength": 30, 
         "minLength": 1
      }
   }
}
```

#### VXLAN-config
```json
{
   "required": ["local_ip", "remote_ip"],
   "type": "object",
   "properties": {
      "local_ip": {
         "title": "Local IP",
         "description": "Local IP of VXLAN interface",
         "type": "string",
         "maxLength": 15, 
         "minLength": 7
      },
      "remote_ip": {
         "title": "Remote IP",
         "description": "Remote IP of VXLAN interface",
         "type": "string",
         "maxLength": 15, 
         "minLength": 7
      }
   }
}
```

### Parameters
| **Parameter** | **Type** | **Comments** |
| ------------- | ------------- | ------------- |
| transaction_id | Integer | The id of the transaction in the CATALYST IT Load Marketplace. |