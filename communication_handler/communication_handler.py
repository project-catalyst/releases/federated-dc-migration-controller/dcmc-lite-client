import os

from communication_handler.api_clients.cloud_openvpn_api import CloudOpenVPNClient
from communication_handler.api_clients.dcmcm_api import DCMCMasterClient
from communication_handler.api_clients.dcmcs_api import DCMCServerClient

communication_handler = None


def get_communication_handler():
    """Get an Instance of the CommunicationHandler Class

    Returns
    -------
    communication_handler : CommunicationHandler
        An instance of the CommunicationHandler Class

    """
    global communication_handler
    if communication_handler is None:
        communication_handler = CommunicationHandler()
    return communication_handler


class CommunicationHandler(object):
    """Communication Handler Class.

    This is a wrapper class for access to all API clients that are necessary for the
    operation of the DCMC Lite component. The API clients can be accessed through
    the properties of this class.

    Attributes
    ----------
    covpn : CloudOpenVPNClient
        An instance of the CloudOpenVPNClient Class
    dcmcm : DCMCMasterClient
        An instance of the DCMCMasterClient Class
    dcmcs : DCMCServerClient
        An instance of the DCMCServerClient Class

    """

    def __init__(self):
        """Communication Handler Constructor Class. """
        self.__covpn_client = None
        self.__dcmcm_client = None
        self.__dcmcs_client = None

    @property
    def covpn(self):
        """Cloud OpenVPN Client. """
        if self.__covpn_client is None:
            self.__covpn_client = CloudOpenVPNClient(cloud_openvpn_url=os.getenv('DCMCL_COVPN_HOST_URL'),
                                                     username=os.getenv('DCMCL_KEYCLOAK_USERNAME'),
                                                     password=os.getenv('DCMCL_KEYCLOAK_PASSWORD'))
        return self.__covpn_client

    @property
    def dcmcm(self):
        """DCMC Master Client. """
        if self.__dcmcm_client is None:
            self.__dcmcm_client = DCMCMasterClient(dcmcm_host_url=os.getenv('DCMCL_DCMCM_HOST_URL'),
                                                   username=os.getenv('DCMCL_KEYCLOAK_USERNAME'),
                                                   password=os.getenv('DCMCL_KEYCLOAK_PASSWORD'))
        return self.__dcmcm_client

    @property
    def dcmcs(self):
        """DCMC Server Client. """
        if self.__dcmcs_client is None:
            self.__dcmcs_client = DCMCServerClient(dcmcs_host_url=os.getenv('DCMCL_DCMCS_HOST_URL'),
                                                   username=os.getenv('DCMCL_KEYCLOAK_USERNAME'),
                                                   password=os.getenv('DCMCL_KEYCLOAK_PASSWORD'))
        return self.__dcmcs_client
