from urllib.parse import urljoin

import requests
from flask_api import status

from .http_client.client import Client


class DCMCServerClient(object):
    """DCMC Server Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Server
    (DCMCS) component of the H2020 CATALYST Migration Controller. It covers calls
    to the part of the API needed by the DCMC Lite Client.

    Methods
    -------
    vcontainer_register(transaction_id)
        Inform DCMC Server of VContainer creation

    """
    __API_PREFIX = 'catalyst/dcmcs/api/'
    __VCONTAINER_CREATED = 'transaction/{transaction_id}/vcontainer/status/created/'
    __CONTROLLER = 'transaction/{transaction_id}/controller/'
    __TOKEN = 'token/'

    def __init__(self, dcmcs_host_url, username, password):
        """DCMC Server Client Class Constructor.

        Parameters
        ----------
        dcmcs_host_url : str
            The host URL of the DCMC Server.
        username : str
            The DCMC Server username
        password : str
            The DCMC Server password

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin(dcmcs_host_url, DCMCServerClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        self.__token = self.token({'username': username, 'password': password})
        self.__headers['Authorization'] = 'Bearer {}'.format(self.__token)

    def token(self, payload):
        """Request token from DCMC Server.

        This service is meant to be used by the DCMC Master Client of the destination DC
        to request an access token on behalf of the new VContainer (which will host the
        migrated load) for accessing the VPN set by the DCMC Server.

        Parameters
        ----------
        payload : dict
            The payload to send to token endpoint. It includes the following keys:
                username : str
                    The username of the issuing DC for gaining authorization in the DCMC Server.
                password : str
                    The password of the issuing DC for gaining authorization in the DCMC Server.

        Returns
        -------
        token : str
            A Keycloak access token

        """
        url = urljoin(self.__url, DCMCServerClient.__TOKEN)
        response = requests.post(url=url, data=payload)
        return response.json()['access_token'] if response.status_code == status.HTTP_201_CREATED else None

    def controller(self, transaction_id):
        """Fetch the OpenStack Controller details.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to fetch controller details for

        Returns
        -------
        Response
            The Response object of the issued request

        """
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCServerClient.__CONTROLLER).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def vcontainer_created(self, transaction_id):
        """Notify DCMC Server of VContainer creation.

        This service is meant to be used by the DCMC Lite Client in order to inform the
        DCMC Server that is has been created for a certain transaction and is ready to
        accept a migrated load.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction in the Catalyst IT Load Marketplace

        Returns
        -------
        Response
            The Response object of the issued request

        """
        params = {'transaction_id': transaction_id}
        # TODO: Remove dummy payload
        payload = {'transaction': transaction_id, 'vcontainer': 'uuid'}
        url = urljoin(self.__url, DCMCServerClient.__VCONTAINER_CREATED).format(**params)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response
