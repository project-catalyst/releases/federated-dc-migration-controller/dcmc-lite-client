from urllib.parse import urljoin

import os
from flask_api import status

from .http_client.client import Client


class CloudOpenVPNClient(object):
    """CloudOpenVPN Client Class.

    This class serves as a client to the API exposed by the Cloud OpenVPN Server
    In the context of the DCMC Lite, only one endpoint is needed, the token.

    Methods
    -------
    token(username, password)
        Retrieve token from Cloud OpenVPN Server
    vpn_by_owner_and_tenant(owner, tenant)
        Retrieve the VPN details (including the certificate) by VPN owner and tenant

    """
    __TOKEN = 'token/'
    __VPN_BY_OWNER_AND_TENANT = 'get/client/{owner}/tenant/{tenant}/vpn/'

    def __init__(self, cloud_openvpn_url, username, password):
        """Cloud OpenVPN Client Class Constructor.

        Parameters
        ----------
        cloud_openvpn_url : str
            The host URL of the Cloud OpenVPN Server.
        username : str
            The username of the VPN creator / owner
        password : str
            The password of the VPN creator / owner

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = cloud_openvpn_url
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        self.__token = self.token(username, password)
        self.__headers['Authorization'] = 'Bearer {}'.format(self.__token)

    def token(self, username, password):
        """Retrieve token from VPN Server.

        Parameters
        ----------
        username : str
            The username of the Cloud OpenVPN user
        password : str
            The password of the Cloud OpenVPN user

        Returns
        -------
        token : str
            A Cloud OpenVPN Keycloak Access Token

        """
        payload = {
            'username': username,
            'password': password
        }
        url = urljoin(self.__url, CloudOpenVPNClient.__TOKEN)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response.json()['access_token'] if response.status_code == status.HTTP_201_CREATED else None

    def vpn_by_owner_and_tenant(self):
        """Retrieve VPN details by connection owner and tenant.

        Returns
        -------
        Response
            A Response object with VPN details

        """
        params = {'owner': os.getenv('DCMCL_COVPN_OWNER'), 'tenant': os.getenv('DCMCL_COVPN_TENANT')}
        url = urljoin(self.__url, CloudOpenVPNClient.__VPN_BY_OWNER_AND_TENANT).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response
