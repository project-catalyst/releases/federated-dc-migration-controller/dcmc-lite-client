from urllib.parse import urljoin

import os

import requests
from flask_api import status

from .http_client.client import Client


class DCMCMasterClient(object):
    """DCMC Master Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Master
    (DCMCM) Client component of the H2020 CATALYST Migration Controller. It covers
    calls to the entire API required by the DCMC Lite Client.

    Methods
    -------
    controller()
        Get the OpenStack Controller details
    vcontainer_registered(transaction_id)
        Inform DCMC Master of VContainer's registration.

    """
    __API_PREFIX = 'catalyst/dcmcm/api/'
    __DCMCS_API_PREFIX = 'catalyst/dcmcs/api/'
    __VCONTAINER_REGISTERED = 'transaction/{transaction_id}/vcontainer/status/registered/'
    __HYPERVISOR_REGISTER = 'hypervisor/register/'
    __SERVICE_ENDPOINTS = 'transaction/{transaction_id}/service-endpoints/'
    __TOKEN = 'token/'

    def __init__(self, dcmcm_host_url, username, password):
        """DCMC Master Client Class Constructor.

        Parameters
        ----------
        dcmcm_host_url : str
            The DCMC Master's Host URL
        username : str
            A DCMC Server username
        password : str
            A DCMC Server password

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin(dcmcm_host_url, DCMCMasterClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        self.__token = self.token(username, password)
        self.__headers['Authorization'] = 'Bearer {}'.format(self.__token)

    @staticmethod
    def token(username, password):
        """Retrieve token from DCMC Server.

        Parameters
        ----------
        username : str
            The Keycloak Username
        password : str
            The Keycloak Password

        Returns
        -------
        Response
            The Response object of the issued request

        """
        payload = {'username': username, 'password': password}
        url = urljoin(os.getenv('DCMCL_DCMCS_HOST_URL'), DCMCMasterClient.__DCMCS_API_PREFIX + DCMCMasterClient.__TOKEN)
        response = requests.post(url=url, data=payload)
        return response.json()['access_token'] if response.status_code == status.HTTP_201_CREATED else None

    def vcontainer_registered(self, transaction_id, payload):
        """Inform DCMCM of VContainer registration.

        The DCMC Lite Client informs the DCMC Master of the source DC that the creation
        of the VContainer has been completed and the virtual OpenStack Compute Node (vCMP)
        has been registered with the controller of the source DC.

        Parameters
        ----------
        transaction_id : int
            The ID of the current transaction
        payload : The payload to send to DCMC Master. It should include the following fields:
            host_name : str
                The name of the Hypervisor's Host
            vpn_ip : str
                The VPN IP of the hypervisor

        Returns
        -------
        Response
            The Response object of the issued request

        """
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCMasterClient.__VCONTAINER_REGISTERED).format(**params)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def hypervisor_register(self, payload):
        """Register self to DCMC Master.

        Parameters
        ----------
        payload : The payload to send to DCMC Master. It should include the following fields:
            host_ip : str
                The IP of the Hypervisor's Host
            host_name : str
                The name of the Hypervisor's Host
            public_ssh_key : str
                The public SSH Key of the Hypervisor's Host

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__HYPERVISOR_REGISTER)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def service_endpoints(self, transaction_id):
        """Retrieve the service endpoints from the controller.

        Parameters
        ----------
        transaction_id : int
            The transaction ID as it appears in the

        Returns
        -------
        Response
            The Response object of the issued request

        """
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCMasterClient.__SERVICE_ENDPOINTS).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response
