.. DCMC Lite Client documentation master file, created by
   sphinx-quickstart on Sat May 16 10:43:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#######################
DCMC Lite Documentation
#######################
This is the documentation for the code implementing the Lite Client of the DC Migration Controller (DCMC) of the
H2020 Catalyst Project. The DCMC Lite resides in every Compute (CMP) node of the CATALYST federation and performs
preparatory tasks for the actual migration, including the connection and disconnection of the CMP node to VPN, the
exchange of SSH-keys between CMP nodes and the VXLAN configurations.

For more details about the role of the DCMC Lite in the overall DC Migration Controller, please refer to H2020 CATALYST
D3.3 *Federated DCs Migration Controller* deliverable report.

.. toctree::
   :maxdepth: 3
   :caption: Table of Contents:

*************************
Installation & Deployment
*************************
In the following, the essentials for the installation of a DCMC Lite instance are described, including
prerequisites for the deployment, the configuration of the services and the actual deployment.

.. toctree::
   :maxdepth: 2

Prerequisites
=============
For the deployment of the DCMC Lite component the Docker engine as well as docker-compose should be installed.
These actions can be performed following the instructions provided below. Firstly, an update should be performed
and essential packages should be installed::

   sudo apt-get update
   sudo apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common

Secondly the key and Docker repository should be added::

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"

Then another update is performed, Docker is installed and the user is added to docker group::

   sudo apt-get update
   sudo apt-get install -y docker-ce
   sudo groupadd docker
   sudo usermod -aG docker $USER

Finally, docker-compose should be installed::

   sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   sudo chmod +x /usr/local/bin/docker-compose

For more information about Docker and docker-compose, please check at https://docs.docker.com/.

Environmental Parameters
========================
The services that are executed as containers, thus forming the DCMC Lite receive their configurations
from a .env file located in the root of the repository. In the following tables, the environmental parameters
that are necessary for the configuration and deployment of the DCMC Master are recorded and described.

|

.. list-table:: Docker and Internal Settings
   :widths: 35 30 35
   :header-rows: 1

   * - Parameter
     - Tag
     - Description
   * - COMPOSE_PROJECT_NAME
     - `docker`
     - The project name (default: catalyst)
   * - DCMCL_DC_HOST_TYPE
     - `DCMC Lite`
     - The host type in the form <deployment_type>:<host_type>. Possible values: *deployment_type*: {all_in_one, distributed}, *host_type*: {physical, virtual}
   * - DCMCL_PHYSICAL_NI
     - `DCMC Lite`
     - The machine's physical network interface
   * - DCMCL_SSH_KEY_DIR
     - `DCMC Lite`
     - SSH directory to mount for SSH-key exchange (e.g. /opt/stack/.ssh)
   * - DCMCL_TUN_DEVICE
     - `DCMC Lite`
     - Temporary settings, until each transaction is mapped to a tun interface

|

.. list-table:: External Services
   :widths: 35 30 35
   :header-rows: 1

   * - Parameter
     - Tag
     - Description
   * - DCMCL_KEYCLOAK_USERNAME
     - `Keycloak`
     - Username
   * - DCMCL_KEYCLOAK_PASSWORD
     - `Keycloak`
     - Password
   * - DCMCL_COVPN_HOST_URL
     - `Cloud OpenVPN`
     - Host URL
   * - DCMCL_COVPN_OWNER
     - `Cloud OpenVPN`
     - Connection Owner
   * - DCMCL_COVPN_TENANT
     - `Cloud OpenVPN`
     - Tenant (default: Catalyst)
   * - DCMCL_DCMCS_HOST_URL
     - `DCMC Server`
     - Host URL

|

Deployment
==========
The DCMC Lite is deployed as a signle Docker container, utilizing docker-compose. Having cloned the code of this
repository, and having created the .env file the following commands should be executed::

   cp .env /dcmc-lite-client
   cd dcmc-lite-client
   docker-compose up --build -d

|

********************
DCMC Lite Client API
********************
This is the documentation of the API offered by the DCMC Lite component. This documentation is composed by:

  * The DCMC Lite Client API;
  * Asynchronous (Celery) Tasks;
  * Essential utilities.

For information about the interaction with other components and their respective implemented API clients please
refer to the next section of this document. For a detailed overview of the available requests offered byt the API
you may refer to the available Swagger documentation, served at http://<HOST_IP>:<HOST_PORT>/catalyst/dcmcl/api/docs.

.. toctree::
   :maxdepth: 2

|

API Documentation
=================

Summary
-------
.. qrefflask:: dcmc_lite_client.dcmcl:app
   :undoc-static:

API Details
-----------
.. autoflask:: dcmc_lite_client.dcmcl:app
   :undoc-static:

|

Asynchronous Tasks
==================
This section includes the documentation for the available asynchronous tasks of DCMC Lite.

.. automodule:: dcmc_lite_client.tasks
   :members:

Essential Utils
===============
This section includes the documentation of the essential utilities of the DCMC Lite Client.

.. automodule:: dcmc_lite_client.utils.miscellaneous
   :members:

****************************
DCMC Lite Client API Wrapper
****************************
This section of the documentation covers the API Wrapper of the DCMC Lite Client for reference purposes.

.. automodule:: dcmc_lite_api_wrapper.dcmcl_api
   :members:

|

*********************
Communication Handler
*********************
In this section, the documentation of the API Clients that are utilized by the DCMC Lite Clients is provided. The DCMCL
utilizes these clients through the Communication Handler Class. All the interactions of the DCMCL are made through this
class. The DCMCL interacts with:

  * The DCMC Server;
  * The DCMC Master Client of the Source DC;
  * The Cloud OpenVPN Server.

In the context of the DCMCL, the API clients include methods for performing operations as needed by the present component
only.

.. automodule:: communication_handler.communication_handler
   :members:

|

.. toctree::
   :maxdepth: 2


DCMC Server Client
==================

.. automodule:: communication_handler.api_clients.dcmcs_api
   :members:

|

DCMC Master Client
==================

.. automodule:: communication_handler.api_clients.dcmcm_api
   :members:

|

Cloud OpenVPN Client
====================

.. automodule:: communication_handler.api_clients.cloud_openvpn_api
   :members:

|


******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
